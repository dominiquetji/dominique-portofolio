package com.example.dotamarketplacepj;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.dotamarketplacepj.database.DatabaseHelper;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Arrays;
public class TopUp extends AppCompatActivity {

    TextView txt_balance,txt_username;
    EditText txtAmount,txtPassword;
    Button topup;
    public static Integer balance = 0;
    DatabaseHelper db;



    final public static ArrayList<Character> alpha = new ArrayList<>(Arrays.asList(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'));
    final public static ArrayList<Character> num = new ArrayList<>(Arrays.asList(
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));
    final public  static ArrayList<String> arrAmount = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up);

        BottomNavigationView btnNav = findViewById(R.id.navbot);
        btnNav.setOnNavigationItemSelectedListener(navListener);
        db = new DatabaseHelper(this);


        txtAmount = findViewById(R.id.txtAmount);
        txtPassword = findViewById(R.id.txtPassword);
        topup = findViewById(R.id.btnTopup);

        txt_username = findViewById(R.id.txt_username);
        txt_username.setText("Hello " + MainForm.username);
        txt_balance = findViewById(R.id.txt_balance);
        txt_balance.setText("Your current Balance:"+ balance);



        topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String balan = txtAmount.getText().toString();
                String pasword = txtPassword.getText().toString();
                Boolean checkPassword = db.checkPassword(pasword);

                AlertDialog.Builder alert = new AlertDialog.Builder(TopUp.this);

                if(balan.isEmpty()){
                    Toast.makeText(TopUp.this, "Top UP amount must be fill", Toast.LENGTH_SHORT).show();
                }else if (!numericc(balan)){
                    Toast.makeText(TopUp.this, "Amount must be nummeric", Toast.LENGTH_SHORT).show();
                } else {
                    int num = Integer.parseInt(balan);
                    if (num < 50000) {
                        Toast.makeText(TopUp.this, "Top Up balance minimum Rp 50.000", Toast.LENGTH_SHORT).show();
                    } else if (pasword.isEmpty()) {
                        Toast.makeText(TopUp.this, "Password must be fill", Toast.LENGTH_SHORT).show();
                    } else if(checkPassword == false){
                        alert.setTitle("Top Up Success ! ");
                        alert.show();
                        balance += num;

                        Intent ref = new Intent(TopUp.this,TopUp.class);
                        startActivity(ref);

                    }else{
                        Toast.makeText(TopUp.this, "Incorrect password", Toast.LENGTH_SHORT).show();

                    }
                }

            }
        });

    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch(item.getItemId()){
                        case R.id.Home :
                            openHome();
                            break;
                        case R.id.History :
                            openHistory();
                            break;
                    }
                    return true;
                }
            };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemsearch:
                Toast.makeText(this,"search item",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.Home:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                openHome();
                return true;
            case R.id.item1:
                Toast.makeText(this, "Top Up", Toast.LENGTH_SHORT).show();
                openTopup();
                return true;
            case R.id.item2:
                Toast.makeText(this,"History",Toast.LENGTH_SHORT).show();
                openHistory();
                return true;
            case R.id.item3:
                Toast.makeText(this,"Log Out",Toast.LENGTH_SHORT).show();
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openHome() {

        Intent intent = new Intent(TopUp.this,MainForm.class);
        intent.putExtra("Balance" , TopUp.balance);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }



    private void openHistory() {
        String username = getIntent().getStringExtra("Username");
        txt_username = findViewById(R.id.txt_username);

        Intent intent= new Intent(this,histori.class);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }

    private void logout() {
        Intent intent= new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    private void openTopup() {

        String username = getIntent().getStringExtra("Username");
        txt_username = findViewById(R.id.txt_username);

        Intent intent= new Intent(this,TopUp.class);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }

    boolean numericc(String num) {
        char[] charNumeric = num.toCharArray();
        for (char a : charNumeric) {
            if (alpha.contains(a)) {
                return false;
            }
        }

        return true;
    }


}