package com.example.dotamarketplacepj;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.dotamarketplacepj.database.DatabaseHelper;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainForm extends AppCompatActivity {

    DatabaseHelper db;
    RecyclerView recyclerView;
    ArrayList<exitem> exitems;
    private static String JSON_URL = "https://gitlab.com/rezaaprian/competitive-programming/-/raw/2fbddccab5cba3e04a8638ff2c920969e2e9c24b/app/src/main/res/raw/contohjson.json";
    exadapter exadapter;

    private TextView txt_username,txt_balance;

    public static String username;
    public static String password;
    public static Integer ns;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_form);

        db = new DatabaseHelper(this);
        BottomNavigationView btnNav = findViewById(R.id.navbot);
        btnNav.setOnNavigationItemSelectedListener(navListener);

        recyclerView = findViewById(R.id.rv_item);
        exitems = new ArrayList<>();
        extractItems();


        txt_username = findViewById(R.id.txt_username);
        txt_balance = findViewById(R.id.txt_balance);

        username = getIntent().getStringExtra("Username");
        password = getIntent().getStringExtra("Password");


        txt_username.setText("Hello  " + username);
        txt_balance.setText("Balance: Rp" + TopUp.balance);

    }

    void add(){
        if(histori.histAR.size() !=0){
            if (ns == null){
                ns = getIntent().getExtras().getInt("StockNew");
                exitems.get(BuyItemForm.pos).setStock(ns);
            }else{
                ns = getIntent().getExtras().getInt("temp");
                exitems.get(BuyItemForm.pos).setStock(ns);
            }

        }
    }

    private void extractItems() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, JSON_URL, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject itemObject = response.getJSONObject(i);

                        exitem item = new exitem();
                        item.setName(itemObject.getString("name").toString());
                        item.setPrice(itemObject.getInt("price"));
                        item.setStock(itemObject.getInt("stock"));
                        item.setLatitude(itemObject.getString("latitude").toString());
                        item.setLongitude(itemObject.getString("longitude").toString());
                        exitems.add(item);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                add();
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                exadapter = new exadapter(exitems, getApplication());
                recyclerView.setAdapter(exadapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TAG", "onErrorResponse: " + error.getMessage());
            }
        });
        queue.add(jsonArrayRequest);

    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch(item.getItemId()){
                        case R.id.Home :
                            openHome();
                            break;
                        case R.id.History :
                            openHistory();
                            break;
                    }
                    return true;
                }
            };



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemsearch:
                Toast.makeText(this,"search item",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.Home:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                openHome();
                return true;
            case R.id.item1:
                Toast.makeText(this, "Top Up", Toast.LENGTH_SHORT).show();
                openTopUp();
                return true;
            case R.id.item2:
                Toast.makeText(this,"History",Toast.LENGTH_SHORT).show();
                openHistory();
                return true;
            case R.id.item3:
                Toast.makeText(this,"Log Out",Toast.LENGTH_SHORT).show();
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openHome() {
        txt_username = findViewById(R.id.txt_username);

        Intent intent= new Intent(this,MainForm.class);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }
    private void openTopUp() {

        Intent intent= new Intent(this,TopUp.class);
        intent.putExtra("Username" , MainForm.username);
        intent.putExtra("Password",MainForm.password);
        startActivity(intent);
    }

    private void openHistory() {

        txt_username = findViewById(R.id.txt_username);

        Intent intent= new Intent(this,histori.class);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }

    private void logout() {
        Intent intent= new Intent(this,MainActivity.class);
        startActivity(intent);
    }

}