package com.example.dotamarketplacepj;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.dotamarketplacepj.database.DatabaseHelper;

import java.util.ArrayList;
import java.util.Arrays;

public class Register extends AppCompatActivity {
    EditText txtFN, txtUN, txtP, txtCP, txtPN;
    CheckBox showP, showCP, cB;
    Button btnR;
    RadioButton btnM, btnF;
    RadioGroup Gender;
    DatabaseHelper db;

    final public static ArrayList<Character> alpha = new ArrayList<>(Arrays.asList(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'));
    final public static ArrayList<Character> num = new ArrayList<>(Arrays.asList(
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));

    final public static ArrayList<String> arrUsername = new ArrayList<>();
    final public static ArrayList<String> arrPassword = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        db = new DatabaseHelper(this);
        txtFN = findViewById(R.id.txtFN);
        txtUN = findViewById(R.id.txtUN);
        txtP = findViewById(R.id.txtP);
        txtCP = findViewById(R.id.txtCP);
        txtPN = findViewById(R.id.txtPN);
        showP = findViewById(R.id.showP);
        showCP = findViewById(R.id.showCP);
        btnR = findViewById(R.id.btnRegister);
        btnF = findViewById(R.id.btnF);
        btnM = findViewById(R.id.btnM);
        Gender = findViewById(R.id.Gender);
        cB = findViewById(R.id.cB);


        showCP.setOnCheckedChangeListener((compoundButton, j) -> {
            if (j) {
                txtCP.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                txtCP.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });
        showP.setOnCheckedChangeListener((compoundButtonn, b) -> {
            if (b) {
                txtP.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                txtP.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });

        btnR.setOnClickListener(v -> {

            String fulname = txtFN.getText().toString();
            String lasname = txtUN.getText().toString();
            String pasword = txtP.getText().toString();
            String confpas = txtCP.getText().toString();
            String phone = txtPN.getText().toString();

            boolean up = !pasword.equals(pasword.toLowerCase());

            AlertDialog.Builder alert = new AlertDialog.Builder(Register.this);


            if (fulname.isEmpty()) {
                Toast.makeText(Register.this, "Full Name must be fill", Toast.LENGTH_SHORT).show();
            } else if (!fulname.contains(" ")) {
                Toast.makeText(Register.this, "Full name must consist 2 words or more", Toast.LENGTH_SHORT).show();
            } else if (fulname.length() < 5 || fulname.length() > 25) {
                Toast.makeText(Register.this, "Full Name Must be between 5 and 25 characters", Toast.LENGTH_SHORT).show();
            } else if (lasname.isEmpty()) {
                Toast.makeText(Register.this, "Username must be fill", Toast.LENGTH_SHORT).show();
            } else if (lasname.length() < 5 || lasname.length() > 25) {
                Toast.makeText(Register.this, "Username Must be between 5 and 25 characters", Toast.LENGTH_SHORT).show();
            } else if(arrUsername.contains(lasname)){
                Toast.makeText(Register.this, "Username must be unique and has not been registered yet", Toast.LENGTH_SHORT).show();
            }else if (pasword.isEmpty()) {
                Toast.makeText(Register.this, "Password must be fill", Toast.LENGTH_SHORT).show();
            } else if (!up) {
                Toast.makeText(Register.this, "Password must contains 1 uppercase ", Toast.LENGTH_SHORT).show();
            } else if(!special(pasword)){
                Toast.makeText(Register.this, "Password must contains 1 Special Characters ", Toast.LENGTH_SHORT).show();
            }else if (pasword.length() > 15) {
                Toast.makeText(Register.this, "Password must be less than 15 characters", Toast.LENGTH_SHORT).show();
            } else if (alphabett(pasword)) {
                alert.setTitle("Error");
                alert.setMessage("Password must include Numeric ");
                alert.show();
            } else if (numericc(pasword)) {
                alert.setTitle("Error");
                alert.setMessage("Password must include Alphabet");
                alert.show();
            } else if(confpas.isEmpty()){
                Toast.makeText(Register.this, "Confirm Password must be fill", Toast.LENGTH_SHORT).show();
            }else if (!confpas.equals(pasword)) {
                Toast.makeText(Register.this, "Password must be the same", Toast.LENGTH_SHORT).show();
            } else if (phone.isEmpty()) {
                Toast.makeText(Register.this, "Phone number must be fill", Toast.LENGTH_SHORT).show();
            } else if (!phone.startsWith("+62")) {
                Toast.makeText(Register.this, "Phone number must starts with  +62 ", Toast.LENGTH_SHORT).show();
            } else if (alphabett(phone)) {
                Toast.makeText(Register.this, "Password must be numeric", Toast.LENGTH_SHORT).show();
            } else if (phone.length() <= 12) {
                Toast.makeText(Register.this, "Phone number must equals 12 after +62", Toast.LENGTH_SHORT).show();
            } else if (Gender.getCheckedRadioButtonId() == -1) {
                Toast.makeText(Register.this, "You must choose gender", Toast.LENGTH_SHORT).show();
            } else if (!cB.isChecked()) {
                Toast.makeText(Register.this, "Agreements must be checked", Toast.LENGTH_SHORT).show();
            } else {

                String gender ="";
                int balance=0;
                if (btnM.isChecked()){
                    gender = "Male";
                }else if(btnF.isChecked()){
                    gender = "Female";
                }
                Boolean checkUsername = db.checkUsername(lasname);
                    if(checkUsername == true) {
                        Boolean insert = db.insert(lasname,pasword,fulname,phone,gender,balance);
                        if (insert == true){
                            Toast.makeText(Register.this, "Register Success", Toast.LENGTH_SHORT).show();
                            arrUsername.add(lasname);
                            arrPassword.add(pasword);
                            openLogin();
                        }else{
                            Toast.makeText(Register.this, "Regiser Failed", Toast.LENGTH_SHORT).show();
                        }
                    } else{
                        Toast.makeText(this, "Username already exists", Toast.LENGTH_SHORT).show();
                    }

            }
            });

    }

    private void openLogin() {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    boolean alphabett(String alp) {

        char[] charAlpha = alp.toCharArray();

        for (char a : charAlpha) {
            if (num.contains(a)) {
                return false;
            }
        }
        return true;

    }

    boolean numericc(String num) {
        char[] charNumeric = num.toCharArray();
        for (char a : charNumeric) {
            if (alpha.contains(a)) {
                return false;
            }
        }

        return true;
    }

    boolean special(String spe) {
        char[] charSpecial = spe.toCharArray();
        for (char a : charSpecial) {
            if ((a >= ' ' && a <= '/')  || (a >= ':' && a <= '@') || (a >= '[' && a <= '`') || (a >= '{' && a <= '~') )
                return true;
        }
        return false;
    }




}




