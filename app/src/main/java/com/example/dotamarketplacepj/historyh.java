package com.example.dotamarketplacepj;

import java.io.Serializable;
import java.util.Date;

public class historyh implements Serializable {

    private Date date;
    private String name;
    private int quan;
    private int min;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuan() {
        return quan;
    }

    public void setQuan(int quan) {
        this.quan = quan;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public historyh(Date date, String name, int quan, int min) {
        this.date = date;
        this.name = name;
        this.quan = quan;
        this.min = min;
    }
}
