package com.example.dotamarketplacepj;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Sellerlocationmap extends FragmentActivity implements OnMapReadyCallback {


    private GoogleMap mMap;
    double latitude;
    double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sellerlocationmap);

        Bundle bundle = getIntent().getBundleExtra("item");
        final exitem items = (exitem) bundle.getSerializable("item");
        latitude = Double.parseDouble(items.getLatitude());
        longitude = Double.parseDouble(items.getLongitude());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng jkt = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(jkt).title("DotaStore"));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(jkt).zoom(20).build();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(jkt));
    }
}

