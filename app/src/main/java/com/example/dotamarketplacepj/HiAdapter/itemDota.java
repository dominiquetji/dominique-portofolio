package com.example.dotamarketplacepj.HiAdapter;

import java.io.Serializable;

public class itemDota implements Serializable {


    private String name;
    private int stock;
    private int price;
    private int thumbnail;


    public itemDota(String name, int stock, int price, int thumbnail) {
        this.name = name;
        this.stock = stock;
        this.price = price;
        this.thumbnail = thumbnail;

    }

    public itemDota() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }


}