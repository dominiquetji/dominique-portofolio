package com.example.dotamarketplacepj.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, "UserDataBase", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("Create table  user(username text primary key , password text , name text, phonenumber text, gender text, balance numeric)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists user");
    }

    // inserting data in database
    public boolean insert(String username,String password,String name,String phonenumber,String gender,Integer balance){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues content = new ContentValues();
        content.put("username",username);
        content.put("password",password);
        content.put("name",name);
        content.put("phonenumber",phonenumber);
        content.put("gender",gender);
        content.put("balance",balance);
        long ins = db.insert("user",null, content);
        if(ins == -1 ) return  false;
        else return  true;
    }

    // checking if username exists
    public Boolean checkUsername (String username){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("Select  * from user  where username=?", new String[]{username});
        if (cur.getCount()>0) return  false;
        else return true;
    }

    //checking if username and password existst
    public Boolean UsernamePassword(String username , String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("Select * from user where username=? and password=?",new String[]{username,password});
        if(cur.getCount()>0) return true;
        else return false;
    }

    public Boolean checkPassword(String password){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("Select * from user where password=?",new String[]{password});
        if (cur.getCount()>0) return false;
        else return true;

    }

}
