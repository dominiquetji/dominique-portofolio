package com.example.dotamarketplacepj;

import java.io.Serializable;

public class exitem implements Serializable {
   private String name;
   private String latitude;
   private String longitude;
   private Integer stock;
   private Integer price;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public exitem(String name, String latitude, String longitude, Integer stock, Integer price) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.stock = stock;
        this.price = price;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public exitem() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


}
