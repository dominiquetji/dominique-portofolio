package com.example.dotamarketplacepj;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class HistoriAdap extends RecyclerView.ViewHolder {

    TextView itemNama,itemQty,itemDate,itemMin;

    public HistoriAdap(@NonNull View itemView) {
        super(itemView);

        itemDate = itemView.findViewById(R.id.item_datee2);
        itemNama = itemView.findViewById(R.id.item_namee2);
        itemQty = itemView.findViewById(R.id.itemquann2);
        itemMin = itemView.findViewById(R.id.itemsum2);
    }
}

class hap extends RecyclerView.Adapter<HistoriAdap>{

    Context ctx;
    ArrayList<historyh> historiAR;

    public hap (Context ctx,ArrayList<historyh> historiAR){
        this.ctx = ctx;
        this.historiAR = historiAR;
    }

    @NonNull
    @Override
    public HistoriAdap onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(ctx).inflate(R.layout.itemhistory,parent,false);
        return new HistoriAdap(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoriAdap holder, int position) {

        final historyh itemHist = historiAR.get(position);

        holder.itemDate.setText("Time : "+ itemHist.getDate());
        holder.itemNama.setText(itemHist.getName());
        holder.itemQty.setText(String.valueOf(itemHist.getQuan()));
        holder.itemMin.setText(String.valueOf(itemHist.getMin()));

    }

    @Override
    public int getItemCount() {
        return historiAR.size();
    }
}