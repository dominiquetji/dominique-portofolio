package com.example.dotamarketplacepj;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class exadapter extends RecyclerView.Adapter<exadapter.ViewHolder> {

        ArrayList<exitem> items;
        private final Context context;

        public exadapter(ArrayList<exitem> items,Context context){
            this.items = items;
            this.context = context;
        }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.itemdoto,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final exitem exitems = items.get(position);

        holder.item_name1.setText(exitems.getName());
        holder.totalstock1.setText(String.valueOf(exitems.getStock()));
        holder.totalprice1.setText(String.valueOf(exitems.getPrice()));

        holder.itembtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,BuyItemForm.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable("item",exitems);
                intent.putExtra("item",bundle);
                intent.putExtra("pos",position);

                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView item_name1,totalstock1,totalprice1;
        public Button itembtn1;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            item_name1 = itemView.findViewById(R.id.item_name1);
            totalstock1 = itemView.findViewById(R.id.totalstock1);
            totalprice1 = itemView.findViewById(R.id.totalprice1);
            itembtn1 = itemView.findViewById(R.id.itembtn1);


        }
    }
}
