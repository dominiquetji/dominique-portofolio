package com.example.dotamarketplacepj;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class BuyItemForm extends AppCompatActivity {
    TextView txt_balance,txt_username,txtTotalPrice;
    Button btnCheckout,btnSeller;
    TextView item_name1,totalstock1,totalprice1;
    ImageView thumbnail1;
    EditText txtQuantity;


    public static Integer sum;
    public static Integer pos;
    Integer jumlah;
    Integer SN;
    Date date = Calendar.getInstance().getTime();


    final public static ArrayList<Character> alpha = new ArrayList<>(Arrays.asList(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'));
    final public static ArrayList<Character> num = new ArrayList<>(Arrays.asList(
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_item_form);

        BottomNavigationView btnNav = findViewById(R.id.navbot);
        btnNav.setOnNavigationItemSelectedListener(navListener);

        requestSendReceieveSMS();

        txtTotalPrice = findViewById(R.id.txtTotalPrice);
        txtQuantity = findViewById(R.id.txtQuantity);
        txt_username = findViewById(R.id.txt_username);
        txt_username.setText("Hello " + MainForm.username);
        txt_balance = findViewById(R.id.txt_balance);
        txt_balance.setText("Your current Balance:Rp" + TopUp.balance);

        btnCheckout = findViewById(R.id.btnCheckout);
        btnSeller = findViewById(R.id.btnSeller);

        thumbnail1 = findViewById(R.id.thumbnail1);
        item_name1 = findViewById(R.id.item_name1);
        totalstock1 = findViewById(R.id.totalstock1);
        totalprice1 = findViewById(R.id.totalprice1);

        Bundle bundle = getIntent().getBundleExtra("item");
        final exitem items = (exitem) bundle.getSerializable("item");

        item_name1.setText(items.getName());
        totalstock1.setText(String.valueOf(items.getStock()));
        totalprice1.setText(String.valueOf(items.getPrice()));

        txtTotalPrice.setText(""+0);

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mesg = "Hello your transaction from DotaMarketPlace is success , Thankyou";
                String balance = txt_balance.getText().toString();
                String qty = txtQuantity.getText().toString();



                if(qty.isEmpty()){
                    Toast.makeText(BuyItemForm.this, "Quantity must be fill", Toast.LENGTH_SHORT).show();
                }else if(!numericc(qty)){
                    Toast.makeText(BuyItemForm.this, "Must be Numeric", Toast.LENGTH_SHORT).show();
                }else {
                    jumlah = Integer.parseInt(qty);
                    sum = items.getPrice() * jumlah;

                    if(jumlah > items.getStock()){
                        Toast.makeText(BuyItemForm.this, "Sorry we don't have another stock besides that was shown in the apps.", Toast.LENGTH_SHORT).show();
                    }else if(TopUp.balance < sum){
                        Toast.makeText(BuyItemForm.this, "Your Balance is not enough,Please TopUp", Toast.LENGTH_SHORT).show();
                    }else{

                        txtTotalPrice.setText("Rp"+sum+",-");
                        pos = getIntent().getExtras().getInt("pos");
                        SN = items.getStock() - jumlah;
                        TopUp.balance -= sum;

                        histori.histAR.add(new historyh(date,items.getName(),jumlah,sum));

                        SmsManager.getDefault().sendTextMessage("5554",null,
                                mesg,null,null);

                        Toast.makeText(BuyItemForm.this, "Your Transaction success", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(BuyItemForm.this,MainForm.class);
                        intent.putExtra("Balance" , balance);
                        intent.putExtra("Username" , MainForm.username);
                        intent.putExtra("Newstock",SN);
                        startActivity(intent);
                    }
                }

            }
        });


        btnSeller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),Sellerlocationmap.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("item",items);
                intent.putExtra("item",bundle);
                startActivity(intent);
            }
        });

    }


    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch(item.getItemId()){
                        case R.id.Home :
                            openHome();
                            break;
                        case R.id.History :
                            openHistory();
                            break;
                    }
                    return true;
                }
            };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemsearch:
                Toast.makeText(this,"search item",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.Home:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                openHome();
                return true;
            case R.id.item1:
                Toast.makeText(this, "Top Up", Toast.LENGTH_SHORT).show();
                openTopup();
                return true;
            case R.id.item2:
                Toast.makeText(this,"History",Toast.LENGTH_SHORT).show();
                openHistory();
                return true;
            case R.id.item3:
                Toast.makeText(this,"Log Out",Toast.LENGTH_SHORT).show();
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openHome() {

        Intent intent = new Intent(this,MainForm.class);
        intent.putExtra("Balance" , TopUp.balance);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }



    private void openHistory() {
        String username = getIntent().getStringExtra("Username");
        txt_username = findViewById(R.id.txt_username);

        Intent intent= new Intent(this,histori.class);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }


    private void logout() {
        Intent intent= new Intent(this,MainActivity.class);
        startActivity(intent);
    }

    private void openTopup() {

        String username = getIntent().getStringExtra("Username");
        txt_username = findViewById(R.id.txt_username);

        Intent intent= new Intent(this,TopUp.class);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }

    private void requestSendReceieveSMS(){
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECEIVE_SMS,Manifest.permission.SEND_SMS},0);
    }



    boolean numericc(String num) {
        char[] charNumeric = num.toCharArray();
        for (char a : charNumeric) {
            if (alpha.contains(a)) {
                return false;
            }
        }

        return true;
    }
}