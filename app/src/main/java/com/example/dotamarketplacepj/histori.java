package com.example.dotamarketplacepj;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class histori extends AppCompatActivity {
    TextView username;
    Button delet;

    RecyclerView historyh;
    RecyclerView.Adapter historiA;
    RecyclerView.LayoutManager lh;

    public static ArrayList<historyh> histAR = new ArrayList<>();

    void init(){
        historyh = findViewById(R.id.rv_item2);
        historiA = new hap(this,histAR);
        lh = new LinearLayoutManager(this);

        historyh.setLayoutManager(lh);
        historyh.setAdapter(historiA);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histori);

        BottomNavigationView btnNav = findViewById(R.id.navbot);
        btnNav.setOnNavigationItemSelectedListener(navListener);

        username = findViewById(R.id.txt_username2);
        delet = findViewById(R.id.delet);

        username.setText("Hello " + MainForm.username);

        delet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            histAR.clear();
            Intent ref = new Intent(histori.this,histori.class);
            startActivity(ref);
            }
        });

        init();


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch(item.getItemId()){
                        case R.id.Home :
                            openHome();
                            break;
                        case R.id.History :
                            openHistory();
                            break;
                    }
                    return true;
                }
            };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemsearch:
                Toast.makeText(this,"search item",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.Home:
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                openHome();
                return true;
            case R.id.item1:
                Toast.makeText(this, "Top Up", Toast.LENGTH_SHORT).show();
                openTopup();
                return true;
            case R.id.item2:
                Toast.makeText(this,"History",Toast.LENGTH_SHORT).show();
                openHistory();
                return true;
            case R.id.item3:
                Toast.makeText(this,"Log Out",Toast.LENGTH_SHORT).show();
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openHome() {
        Intent intent= new Intent(this,MainForm.class);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }

    private void openTopup() {

        String password = getIntent().getStringExtra("Password");

        Intent intent= new Intent(this,TopUp.class);
        intent.putExtra("Username" , MainForm.username);
        intent.putExtra("Password",MainForm.password);
        startActivity(intent);
    }

    private void openHistory() {

        Intent intent= new Intent(this,histori.class);
        intent.putExtra("Username" , MainForm.username);
        startActivity(intent);
    }

    private void logout() {
        Intent intent= new Intent(this,MainActivity.class);
        startActivity(intent);
    }

}